const s3Service = require('../services/S3');

module.exports = (app) => {
    app.get('/splitBbn', async (req, res, next) => {
        const params = {
            Bucket: process.env.BUCKET,
            Key: process.env.BUCKET_LINK
        };
        const data = await s3Service.getData(params);
        let i = 0;
        let max = parseInt(process.env.CHUNK_SIZE);
        const totalPage = Math.ceil(data.length / max);
        for (let page = 0; page < totalPage; page++) {
            await s3Service.jsonToCsv(page, 'split' ,data.slice(i, Number(i + max)));
            i = i + max;
        }

        res.status(200).send(data);
    });
}
