const AWS = require("aws-sdk");
const csv = require("csvtojson");
const converter = require("json-2-csv");
const {writeFileSync} = require("fs");

class Service {
    #S3;
    constructor() {
        this.#S3 = new AWS.S3();
    }
    async getData(params) {
        const stream = this.#S3.getObject(params).createReadStream();
        return csv({output:"line"}).fromStream(stream);
    }

    async jsonToCsv(index, name, itemsJson) {
        try {
            const dirPath = process.env.CHUNK_LINK;
            const dateTime = new Date().getTime().toString();
            const fileExtension = ".csv";
            const fileName = `bbn_chunk_${index}_${dateTime}${fileExtension}`;
            const finalName = fileName;
            const csv = await converter.json2csv(itemsJson);
            writeFileSync(finalName, csv)

            return fileName;
        } catch (error) {
            console.log("convertJsonToFileCsv: ", error);
        }
    }
}

module.exports = new Service();
