'use strict';

const express = require('express');
const sls = require('serverless-http');
const app = express();
require('./controllers/split-bbn')(app);

module.exports.server = sls(app);